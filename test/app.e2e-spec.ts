import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import {
  CommissionResponse,
  IComRequest,
} from '../src/commission/commission.types';
import { RatesResponse } from 'src/exchange/exchange.types';
import * as nock from 'nock';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  describe('GET /commission', () => {
    it('returns the properly formatted response when request is valid', async () => {
      const req: IComRequest = {
        client_id: 42,
        date: '2012-01-01',
        amount: '2000.00',
        currency: 'EUR',
      };
      const expectedRes: CommissionResponse = {
        currency: 'EUR',
        amount: '0.05',
      };
      const res = await request(app.getHttpServer())
        .post('/commission')
        .send(req);
      expect(res.status).toEqual(201);
      expect(res.body).toEqual(expectedRes);
    });
    it('rejects missformatted input', async () => {
      const req: IComRequest = {
        client_id: 42,
        date: 'yesterday',
        amount: '2000',
        currency: 'ABC',
      };
      const res = await request(app.getHttpServer())
        .post('/commission')
        .send(req);
      expect(res.status).toEqual(400);
      expect(res.body.message).toHaveLength(3);
    });
    it('handles currency calculations well', async () => {
      const ratesResponse: RatesResponse = {
        motd: {
          url: 'fake.com',
          msg: 'fake message',
        },
        success: true,
        base: 'EUR',
        date: '2022-01-01',
        historical: true,
        rates: {
          PLN: 4.570522,
          USD: 1.123456,
          GBP: 0.989754,
        },
      };
      nock('https://api.exchangerate.host')
        .get('/2021-01-01')
        .reply(200, ratesResponse);
      const req: IComRequest = {
        client_id: 1,
        date: '2012-01-01',
        amount: '2000.00',
        currency: 'PLN',
      };
      const expectedRes: CommissionResponse = {
        currency: 'EUR',
        amount: '2.19',
      };
      const res = await request(app.getHttpServer())
        .post('/commission')
        .send(req);
      expect(res.status).toEqual(201);
      expect(res.body).toEqual(expectedRes);
    });
  });
});
