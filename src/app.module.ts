import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ExchangeModule } from './exchange/exchange.module';
import { CommissionModule } from './commission/commission.module';

@Module({
  imports: [ConfigModule.forRoot(), ExchangeModule, CommissionModule],
})
export class AppModule {}
