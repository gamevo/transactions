import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { RatesResponse } from './exchange.types';

const RATES_ENDPOINT = process.env.EXCHANGE_API_ENDPOINT
  ? process.env.EXCHANGE_API_ENDPOINT
  : 'https://api.exchangerate.host/2021-01-01';
const PRECISION = process.env.PRECISION ? parseInt(process.env.PRECISION) : 6;

@Injectable()
export class ExchangeService {
  constructor(private httpService: HttpService) {}
  /**
   * Call external service for currency exchange rate againt EUR
   * @param currencyCode ISO3 currency code
   * @returns async number in raw format. Remember to do calculations parsed to int
   */
  async getRateToEUR(currencyCode: string): Promise<number> {
    const apiResponse = await this.httpService.get(RATES_ENDPOINT).toPromise();
    const rawData = (await apiResponse.data) as RatesResponse;
    return rawData.rates[currencyCode];
  }

  /**
   * Converts value to EUR and formats nicely.
   * @param amount
   * @param rateBaseEUR use getRateToEUR
   * @returns formatted eur value 2-decimals
   */
  convertToEUR(amount: string, rateBaseEUR: number): string {
    const multiplier = Math.pow(10, PRECISION);
    const valueBig = Math.round(parseFloat(amount) * multiplier);
    const rateBig = Math.round(rateBaseEUR * multiplier);
    return this.formatToMoney(valueBig / rateBig);
  }

  formatToMoney(value: number): string {
    return value.toFixed(2);
  }
}
