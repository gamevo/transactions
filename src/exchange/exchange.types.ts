export const currencyCodes = <const>[
  'EUR',
  'PLN',
  'USD',
  'GBP',
]; /* TODO: complete list of currencies*/

export type CurrenciesTuple = typeof currencyCodes;
export type Currency = CurrenciesTuple[number];

export type Rates = {
  [key in Currency]?: number;
};
export interface Motd {
  msg: string;
  url: string;
}

export interface RatesResponse {
  motd: Motd;
  success: boolean;
  historical: boolean;
  base: string /* may need a closed list of supported currencies */;
  date: string;
  rates: Rates;
}
