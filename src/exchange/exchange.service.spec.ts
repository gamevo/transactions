import { HttpModule } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { ExchangeService } from './exchange.service';
import * as nock from 'nock';
import { RatesResponse } from './exchange.types';

describe('ExchangeService', () => {
  let service: ExchangeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [ExchangeService],
    }).compile();

    service = module.get<ExchangeService>(ExchangeService);
  });

  describe('getRateToEUR', () => {
    it('calls exchange API and returns a currency rate', async () => {
      const ratesResponse: RatesResponse = {
        motd: {
          url: 'fake.com',
          msg: 'fake message',
        },
        success: true,
        base: 'EUR',
        date: '2022-01-01',
        historical: true,
        rates: {
          PLN: 4.570522,
          USD: 1.123456,
          GBP: 0.989754,
        },
      };
      nock('https://api.exchangerate.host')
        .get('/2021-01-01')
        .reply(200, ratesResponse);
      const response = await service.getRateToEUR('PLN');
      expect(response).toEqual(4.570522);
    });
  });

  describe('convertToEUR', () => {
    it('properly multiples rate by amount and formats to 2 decimals', () => {
      expect(service.convertToEUR('468.12', 4.123456)).toEqual('113.53');
    });
  });
});
