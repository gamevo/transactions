import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ExchangeService } from './exchange.service';

@Module({
  imports: [HttpModule],
  providers: [ExchangeService],
  exports: [ExchangeService],
})
export class ExchangeModule {}
