import { Injectable } from '@nestjs/common';
import {
  ClientTurnover,
  CommissionResponse,
  IComRequest,
  IRule,
} from './commission.types';
import * as money from 'money-math';
import { ExchangeService } from '../exchange/exchange.service';

@Injectable()
export class CommissionService {
  private turnoverCache: ClientTurnover[];
  private commissionRules: IRule[];

  private defaultRule: IRule = {
    matchFun: async () => {
      return true;
    },
    discountFun: (r) => {
      const DEFAULT_RATE = '0.50';
      const MIN_COMMISION = '0.05';
      const defaultCommission = money.percent(r.amount, DEFAULT_RATE);
      return defaultCommission < MIN_COMMISION
        ? MIN_COMMISION
        : defaultCommission;
    },
  };

  private client42Rule: IRule = {
    matchFun: async (r) => {
      return r.client_id === 42;
    },
    discountFun: () => {
      return '0.05';
    },
  };

  private turnoverRule: IRule = {
    matchFun: async (r) => {
      const TURNOVER_THREASHOLD = 1000;
      const clientTurnover = await this.getClientTurnover(
        r.client_id,
        this.getMonth(r.date),
      );
      return parseInt(clientTurnover) >= TURNOVER_THREASHOLD;
    },
    discountFun: () => {
      return '0.03';
    },
  };

  constructor(private readonly exchangeService: ExchangeService) {
    /*
    Sequence matters. Discount functions should be sorted from cheapest to the most expensive.
    This way we don't need to compare discounts calculated with all rules.
    In case you need to extend the list of rules so that the order is no longer obvious,
    Refactor the calculateCommision with Promise.all(commissionRules.map...) and find min
    */
    this.commissionRules = [
      this.turnoverRule,
      this.client42Rule,
      this.defaultRule,
    ];

    this.turnoverCache = [];
  }
  /**
   * Calculates the commission for transaction. Updates the turnaround cache for each client / month.
   * @param request raw request for commission
   * @returns formatted response
   */
  async calculateCommission(request: IComRequest): Promise<CommissionResponse> {
    const eurTransaction = await this.standardizeTransactionCurrency(request);
    const commisionRule = await this.findRuleForTransaction(request);
    const commission = commisionRule.discountFun(eurTransaction);
    await this.updateTurnover(request);
    return {
      amount: commission,
      currency: 'EUR',
    };
  }

  /**
   * Get a turnover. This should call a repository of transactions or a transaction service.
   * Very simple implementation using cache
   * @param clientId
   * @param month
   * @returns a number async to simmulate ext service
   */
  async getClientTurnover(clientId: number, month: number): Promise<string> {
    const turnover = this.turnoverCache.find(
      (t) => t.clientId === clientId && t.month === month,
    );
    return turnover ? turnover.amount : '0';
  }

  /**
   * Update turnover. This should not be handled in commissions service.
   * Calculating commission should happen before transaction is committed.
   * @param clientId
   * @param month
   * @param value
   */
  async saveClientTurnover(
    clientId: number,
    month: number,
    value: string,
  ): Promise<void> {
    const future: ClientTurnover = {
      clientId,
      month,
      amount: value,
    };
    this.turnoverCache = this.turnoverCache.filter(
      (t) => !(t.clientId === clientId && t.month === month),
    );
    this.turnoverCache.push(future);
  }

  async updateTurnover(r: IComRequest): Promise<void> {
    const month = this.getMonth(r.date);
    const before = await this.getClientTurnover(r.client_id, month);
    const after = money.add(before, r.amount);
    await this.saveClientTurnover(r.client_id, month, after);
  }

  getMonth(rawDate: string): number {
    const date = new Date(rawDate);
    return date.getMonth();
  }

  async standardizeTransactionCurrency(r: IComRequest): Promise<IComRequest> {
    if (r.currency === 'EUR') {
      return r;
    }
    const exchangeRate = await this.exchangeService.getRateToEUR(r.currency);
    const amountEUR = this.exchangeService.convertToEUR(r.amount, exchangeRate);

    return {
      client_id: r.client_id,
      currency: 'EUR',
      date: r.date,
      amount: amountEUR,
    };
  }

  async findRuleForTransaction(tran: IComRequest): Promise<IRule> {
    for (const r of this.commissionRules) {
      if (await r.matchFun(tran)) {
        return r;
      }
    }
    return this
      .defaultRule; /* just in case somebody messes with functions too much*/
  }
}
