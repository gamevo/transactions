import { IsEnum, IsInt, Matches } from 'class-validator';
import { currencyCodes } from '../exchange/exchange.types';

export interface IComRequest {
  date: string;
  amount: string;
  currency: string;
  client_id: number;
}

export class CommissionRequest implements IComRequest {
  @Matches(/\d{4}-\d{2}-\d{2}/)
  date: string;
  @Matches(/^[0-9]*\.[0-9]{2}$/)
  amount: string;
  @IsEnum(currencyCodes)
  currency: string;
  @IsInt()
  client_id: number;
}

export interface CommissionResponse {
  amount: string;
  currency: string;
}

export type ClientTurnover = {
  clientId: number;
  month: number;
  amount: string;
};

export interface IRule {
  matchFun: (
    request: IComRequest,
  ) => Promise<boolean> /* needs to be async as checking for turnaround is */;
  discountFun: (request: IComRequest) => string;
}
