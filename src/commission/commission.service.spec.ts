import { Test, TestingModule } from '@nestjs/testing';
import { ExchangeModule } from '../exchange/exchange.module';
import { CommissionService } from './commission.service';
import { ExchangeService } from '../exchange/exchange.service';

describe('CommissionService', () => {
  let service: CommissionService;
  let exService: ExchangeService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ExchangeModule],
      providers: [CommissionService],
    }).compile();

    service = module.get<CommissionService>(CommissionService);
    exService = module.get<ExchangeService>(ExchangeService);
    jest.spyOn(exService, 'getRateToEUR').mockImplementation(async () => 1.1);
  });

  describe('calculateCommission', () => {
    it('applies special discount to client 42', async () => {
      const result = await service.calculateCommission({
        client_id: 42,
        currency: 'EUR',
        date: '2021-01-02',
        amount: '2000.00',
      });
      expect(result.amount).toEqual('0.05');
    });
    it('applies default commission', async () => {
      const result = await service.calculateCommission({
        client_id: 1,
        currency: 'EUR',
        date: '2021-01-03',
        amount: '500.00',
      });
      expect(result.amount).toEqual('2.50');
    });
    it('rounds the commission nicely', async () => {
      const result = await service.calculateCommission({
        client_id: 1,
        currency: 'EUR',
        date: '2021-01-04',
        amount: '499.00',
      });
      expect(result.amount).toEqual('2.50');
    });
    it('applies default commission until turnaround threashold', async () => {
      const result = await service.calculateCommission({
        client_id: 1,
        currency: 'EUR',
        date: '2021-01-04',
        amount: '100.00',
      });
      expect(result.amount).toEqual('0.50');
    });
    it('applies lower commission after turnaround threashold', async () => {
      const result = await service.calculateCommission({
        client_id: 1,
        currency: 'EUR',
        date: '2021-01-05',
        amount: '1.00',
      });
      expect(result.amount).toEqual('0.03');
    });
    it('applies default commission in next month', async () => {
      const result = await service.calculateCommission({
        client_id: 1,
        currency: 'EUR',
        date: '2021-02-01',
        amount: '500.00',
      });
      expect(result.amount).toEqual('2.50');
    });
  });
});
