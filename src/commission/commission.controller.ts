import { Body, Controller, Post } from '@nestjs/common';
import { CommissionService } from './commission.service';
import { CommissionRequest, CommissionResponse } from './commission.types';

@Controller('commission')
export class CommissionController {
  constructor(private readonly service: CommissionService) {}

  @Post('')
  async getCommission(
    @Body() data: CommissionRequest,
  ): Promise<CommissionResponse> {
    return this.service.calculateCommission(data);
  }
}
