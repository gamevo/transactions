# Commission calculation service
Welcome to the service that calculates commissions for transactions.

## Setup
To configure the project you just need node 14+ and npm 6+\
`npm install`

## Run
`npm start`

## Testing
Like in any nest.js project tests are divided into:
 - unit `npm run test`
 - e2e `npm run test:e2e`

## Remarks
### Calculating turnover in a month
The turnoverCache in commissions service is ugly. Setting up a micro database for storing transactions for a service that does commissions makes no sense for me. I expect there should be a mini service responsible for executing and recording transactions. It's much more reliable to use such service then repeat the turnover calculation in various services. This service is easy to modify to use transaction service.

### Money
Ultimately I decided to use existing library rather than reinvent it. It passes money as strings with quite strict formatting rules. The money-math library does not handle values with more than 2 digits precision well. There is a little function responsible for currency calcuations and formatting.

## Feedback
There are definetly many aspects of the code that could be done better. I'm happy to hear about all of them. I don't get offended easily:)
